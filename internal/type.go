package internal

import "fmt"

type AnySwapResponse struct {
	Msg  string `json:"msg"`
	Info []Transaction `json:"info"`
}

type Transaction struct {
	ID              string  `json:"_id"`
	From            string  `json:"from"`
	Bind            string  `json:"bind"`
	SrcChainID      string  `json:"srcChainID"`
	DestChainID     string  `json:"destChainID"`
	PairID          string  `json:"pairid"`
	Timestamp       int64   `json:"timestamp"`
	FormatSwapValue float32 `json:"formatswapvalue"`
}

type AnySwapStats struct {
	AllTxns		int64
}

func (a AnySwapResponse) Print() {
	fmt.Println("msg: ", a.Msg)
	for i:= 0; i < len(a.Info); i++ {
		fmt.Println(
			"\n_id ", a.Info[i].ID, 
			"\nfrom ", a.Info[i].From,
			"\nbind ", a.Info[i].Bind,
			"\ntimestamp ", a.Info[i].Timestamp,
			"\npairid ", a.Info[i].PairID,
			"\nsrcChainID ", a.Info[i].SrcChainID,
			"\ndestChainID ", a.Info[i].DestChainID,
			"\nformatswapvalue ", a.Info[i].FormatSwapValue,
			)
		fmt.Println()
	}
}