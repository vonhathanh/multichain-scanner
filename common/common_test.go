package common

import (
	"encoding/hex"
	"testing"
)

func TestDecodeHexString(t *testing.T) {
	s, err := hex.DecodeString("0x542fe1c45fc70578110f3e3e4565dbbeaa86a1f996c024ee71a6606b570711f6")
	t.Log(s, err)
	s, err = hex.DecodeString("542fe1c45fc70578110f3e3e4565dbbeaa86a1f996c024ee71a6606b570711f6")
	t.Log(s, err)
}