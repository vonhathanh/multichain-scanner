package internal

type DB interface {
	InsertBatchTxs(txs *AnySwapResponse)
	GetProcessedTxsCount() (int64, error)
	UpdateNumProcessedTxs(number int64)
}