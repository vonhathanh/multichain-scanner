package postgres

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"strconv"

	_ "github.com/lib/pq"
	"gitlab.com/vonhathanh/multichain-scanner/common"
	"gitlab.com/vonhathanh/multichain-scanner/internal"
)

type PostgresDB struct {
	SQL *sql.DB
}

func connect(host, port, user, password, dbname string) *PostgresDB {
	connStr := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
							host, port, user, password, dbname)

	db, err := sql.Open("postgres", connStr)	
	if err != nil {
		panic(err)
	}			

	var Postgres = &PostgresDB{}

	Postgres.SQL = db
	return Postgres
}

func (p *PostgresDB) insertTx(tx internal.Transaction) error {
	insertStatement := `INSERT INTO transaction ("id", "from", "to", "token", "value", "timestamp", "srcChainID", "destChainID") VALUES ($1, $2, $3, $4, $5, $6, $7, $8)`
	var (
		from, _ = common.DecodeHexString(tx.From)
		to, _ = common.DecodeHexString(tx.Bind)
		id, _ = common.DecodeHexString(tx.ID)
		srcChainId, _ = strconv.Atoi(tx.SrcChainID)
		destChainID, _ = strconv.Atoi(tx.DestChainID)
	)
	_, err := p.SQL.Exec(insertStatement, id, from, to, tx.PairID, tx.FormatSwapValue, tx.Timestamp, srcChainId, destChainID)	
	if err != nil {
		log.Println("Insert tx to db: ", err.Error())
		return err
	}

	return nil
}

func (p *PostgresDB) InsertBatchTxs(txs *internal.AnySwapResponse) {
	for _, tx := range txs.Info {
		p.insertTx(tx)
	}
}

func (p *PostgresDB) GetProcessedTxsCount() (int64,error) {
	statement := "SELECT * FROM metadata"
	var numProcessedTxs int64
	rows, err := p.SQL.Query(statement)

	if err != nil {
		log.Println(err)
	}

	if rows.Next() {
		if err := rows.Scan(&numProcessedTxs); err != nil {
			log.Println(err)
		}
	}
	return numProcessedTxs, err
}

func (p *PostgresDB) UpdateNumProcessedTxs(number int64) error {
	insertStatement := `UPDATE metadata SET processed_tx = $1`

	_, err := p.SQL.Exec(insertStatement, number)	
	if err != nil {
		log.Println("Insert tx to db: ", err.Error())
		return err
	}

	return nil
}

func NewDB() *PostgresDB {
	return connect("127.0.0.1", "5432", "postgres", os.Getenv("PATH_FINDER_DB_PASSWORD"), "postgres")
}