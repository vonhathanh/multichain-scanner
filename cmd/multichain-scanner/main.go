package main

import (
	"gitlab.com/vonhathanh/multichain-scanner/anyswap"
	"gitlab.com/vonhathanh/multichain-scanner/postgres"
)


func main() {
	db := postgres.NewDB()
	anyswap.CatchUp(db)
}