package common

import "encoding/hex"

func DecodeHexString(s string) ([]byte, error) {
	if has0xPrefix(s) {
		return hex.DecodeString(s[2:])
	}
	return hex.DecodeString(s)
}

func has0xPrefix(input string) bool {
	return len(input) >= 2 && input[0] == '0' && (input[1] == 'x' || input[1] == 'X')
}