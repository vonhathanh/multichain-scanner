package anyswap

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"gitlab.com/vonhathanh/multichain-scanner/common"
	"gitlab.com/vonhathanh/multichain-scanner/internal"
	"gitlab.com/vonhathanh/multichain-scanner/postgres"
)


func Download(offset int64, limit int64) *internal.AnySwapResponse {
	response, err := http.Get(fmt.Sprintf("https://bridgeapi.anyswap.exchange/v2/all/history/all/all/all/all?offset=%d&limit=%d&status=10", offset, limit))
	if err != nil {
		log.Fatal(err)
	}
	defer response.Body.Close()

	var resp internal.AnySwapResponse

	if err = json.NewDecoder(response.Body).Decode(&resp); err != nil {
		log.Fatal(err)
	}
	return &resp
}

func getAnySwapStats(url string, c chan int64) {
	response, err := http.Get(url)
	if err != nil {
		log.Fatal(err)
	}

	var resp internal.AnySwapStats

	if err = json.NewDecoder(response.Body).Decode(&resp); err != nil {
		log.Fatal(err)
	}
	c <- resp.AllTxns
}

func GetTotalTxs() int64 {
	c := make(chan int64)

	go getAnySwapStats("https://bridgeapi.anyswap.exchange/data/stats", c)
	go getAnySwapStats("https://bridgeapi.anyswap.exchange/data/stats/stable", c)

	normalTxCount, stableTxCount := <-c, <-c

	return normalTxCount + stableTxCount
}

func CatchUp(db *postgres.PostgresDB) {
	numTotalTx := GetTotalTxs()
	start, _ := db.GetProcessedTxsCount()

	if start >= numTotalTx {
		return
	}

	for i := start; i < numTotalTx; i += common.NUM_TX_TO_PROCESS_PER_BATCH {
		resp := Download(numTotalTx - i, common.NUM_TX_TO_PROCESS_PER_BATCH)
		db.InsertBatchTxs(resp)
		db.UpdateNumProcessedTxs(i + common.NUM_TX_TO_PROCESS_PER_BATCH)
		log.Printf("Processed %d txs, we are at: %d\n", common.NUM_TX_TO_PROCESS_PER_BATCH, i + common.NUM_TX_TO_PROCESS_PER_BATCH)
	}
}